package basic;

import java.util.ArrayList;
import java.util.List;

public class MainClass {

	public static void main(String[] args) {
		
		//Question 1
		System.out.println("Name= "+Service.print());
		
		//Question 2
		System.out.println("Two number: "+Service.addNumber(5, 10));
		
		// Question 6
		System.out.println("Three number: "+Service.addNumber(5, 10,10));
		
		//Question 9
		Service service = new Service();
		service.addEmployee();
		List<Employee> employees = new ArrayList<Employee>();
		employees=service.printEmployee();
		System.out.println(service.searchById(2));
		
		// Queston 10
		System.out.println("Object: "+service.searchById1(1));

		//Question 11
		double diff = service.salaryDifference(employees.get(0), employees.get(1));
		
		//Question 12
		System.out.println(diff);
	}
}
