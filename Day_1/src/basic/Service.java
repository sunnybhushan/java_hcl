package basic;

import java.util.ArrayList;
import java.util.List;

public class Service {

	List<Employee> employeeList = new ArrayList<Employee>();

	static boolean print() {
		for (int i = 0; i <= 4; i++) {
			System.out.println("Bhushan");
		}
		return true;
	}

	static int addNumber(int a, int b) {
		int c = a + b;
		return c;
	}

	static int addNumber(int a, int b, int c) {
		int d = a + b + c;
		return d;
	}

	void addEmployee() {
		Employee emp = new Employee(1, "Bhushan", "Patna", 200);
		Employee emp1 = new Employee(2, "Ram", "Ayodhya", 500);
		employeeList.add(emp);
		employeeList.add(emp1);

	}

	List printEmployee() {
		System.out.println(employeeList);
		return employeeList;
	}

	boolean searchById(int id) {
		for (Employee empObj : employeeList) {
			if (id == (empObj.getId())) {
				return true;
			}
		}
		return false;
	}

	Employee searchById1(int id) {
		for (Employee empObj : employeeList) {
			if (id == (empObj.getId())) {
				return empObj;
			}
		}
		return null;
	}

	double salaryDifference(Employee emp, Employee emp1) {
		double salary;
		if (emp.getSalary() > (emp1.getSalary())) {
			System.out.println("Employee 1 is greater");
			salary = emp.getSalary() - emp1.getSalary();
		} else {
			System.out.println("Employee 2 is greater");
			salary = emp1.getSalary() - emp.getSalary();
		}
		return salary;
	}

}
