/**
* @author  Sunny Bhushan
* @version 1.0
* @since   2022-01-20 
*/

package com.hcl.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.hcl.model.Address;
import com.hcl.model.Department;
import com.hcl.model.Employee;
import com.hcl.service.Service;


public class MainApp {
	public static void main(String[] args) throws Exception {

		Address address = new Address(1, "Chennai", "Tamil Naidu", "India");
		Address address1 = new Address(2, "Patna", "Bihar", "India");

		Address[] addresses = new Address[2];
		addresses[0] = address;
		addresses[1] = address1;

		Employee employee = new Employee(101, "Bhushan", addresses);
		Employee employee1 = new Employee(102, "Ravi", addresses);

		Employee[] employees = new Employee[2];
		employees[0] = employee;
		employees[1] = employee1;

		String string = "2012-12-31";
		DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		java.util.Date date = formatter.parse(string);
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());

		Department dept = new Department(111, "MCA", employees, sqlDate);// full
		Department dept1 = new Department(222, "BCA", employees, sqlDate);

		Department[] departments = new Department[2];
		departments[0] = dept;
		departments[1] = dept1;

		Service service = new Service();

		/** In department search for Employee name */
		Employee[] emp3 = service.searchEmployeeInDepartment(departments, "Bhushan");

		if (emp3 != null) {
			for (Employee employee2 : emp3) {
				if (employee2 != null)
					System.out.println(employee2);
			}
		} else
			System.out.println("No data available");

		System.out.println("Second---------------");

		/** In department search for Employee location's city */
		boolean emp4 = service.searchEmployee(departments, "Chennai");// array 
		if (emp4)
			System.out.println("Employee is there");
		else
			System.out.println("No data available");
		
		//Service service = null;

	}
}
