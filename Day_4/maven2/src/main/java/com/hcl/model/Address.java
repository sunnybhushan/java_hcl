/**
* @author  Sunny Bhushan
* @version 1.0
* @since   2022-01-20 
*/

package com.hcl.model;

public class Address {
	
	private int addressId;
	private String city;
	private String state;
	private String country;
	
	public Address(int addressId, String city, String state, String country) {
		super();
		this.addressId = addressId;
		this.city = city;
		this.state = state;
		this.country = country;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", city=" + city + ", state=" + state + ", country=" + country + "]";
	}
	
	
}
