/**
* @author  Sunny Bhushan
* @version 1.0
* @since   2022-01-20 
*/
package com.hcl.model;

import java.util.Arrays;
import java.sql.Date;

public class Department {

	private int deptId;
	private String name;
	private Employee[] employee;
	private Date date;

	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee[] getEmployee() {
		return employee;
	}

	public void setEmployee(Employee[] employee) {
		this.employee = employee;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Department(int deptId, String name, Employee[] employee, Date date) {
		super();
		this.deptId = deptId;
		this.name = name;
		this.employee = employee;
		this.date = date;
	}

	@Override
	public String toString() {
		return "Department [deptId=" + deptId + ", name=" + name + ", employee=" + Arrays.toString(employee) + ", date="
				+ date + "]";
	}

}
