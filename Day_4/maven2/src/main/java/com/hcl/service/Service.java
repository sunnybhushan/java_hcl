/**
* @author  Sunny Bhushan
* @version 1.0
* @since   2022-01-20 
*/
package com.hcl.service;

import com.hcl.model.Address;
import com.hcl.model.Department;
import com.hcl.model.Employee;

public class Service {

	public Employee[] searchEmployeeInDepartment(Department[] departments, String name) {

		Employee[] employees = new Employee[5];
		int count = 0;
		for (Department department : departments) {
			for (Employee employess : department.getEmployee()) {
				if (employess.getName() == (name)) {
					employees[count] = employess;
					count++;
				}
			}
		}

		return employees;
	}

	public boolean searchEmployee(Department[] departments, String city) {

		for (Department department : departments) {
			for (Employee employess : department.getEmployee()) {
				for (Address address : employess.getAddress())
					if (address.getCity().equalsIgnoreCase(city))
						return true;
			}
		}

		return false;
	}

}
