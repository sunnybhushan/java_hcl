package accountBO;

import com.exception.AccountBOException;
import com.exception.AccountDAOException;

import accountDAO.AccountDAO;
import accountVO.AccountVO;

public class AccountBO {

	public boolean storeAccountDetails(AccountVO vo) throws AccountBOException {
		try {
			AccountDAO dao = new AccountDAO();
			boolean flag;
			flag = dao.storeAccountDetails(vo);
			return flag;
		} catch (AccountDAOException e) {
			throw new AccountBOException("Error when adding", e);
		}
	}

	public AccountVO getAccountDetails(int account_number) throws AccountBOException {

		try {
			AccountDAO dao = new AccountDAO();
			AccountVO vo = new AccountVO();
			vo = dao.getAccountDetails(account_number);
			return vo;
		} catch (AccountDAOException e) {
			throw new AccountBOException("Error when getting account details");
		}
	}

}
