package accountDAO;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.exception.AccountDAOException;

import accountVO.AccountVO;

public class AccountDAO {

	public boolean storeAccountDetails(AccountVO vo) throws AccountDAOException {

		String userName = "root";
		String password = "";
		String url = "jdbc:mysql://localhost:3306/sample";
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
			Driver driver = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(driver);
			connection = DriverManager.getConnection(url, userName, password);
			String query = "insert into account (account_number, customer_name,"
					+ " contact_no,aadhar_no,customer_dob,account_balance)" + " "
							+ "values (?,?,?,?,?,?)";
			stmt = connection.prepareStatement(query);
			stmt.setInt(1, vo.getAccount_number());
			stmt.setString(2, vo.getCustomer_name());
			stmt.setInt(3, vo.getContact_no());
			stmt.setInt(4, vo.getAadhar_no());
			stmt.setDate(5, vo.getCustomer_dob());
			stmt.setDouble(6, vo.getAccount_balance());
			stmt.executeUpdate();
			System.out.println("Inserted successful");
			flag = true;
		} catch (SQLException e) {
			throw new AccountDAOException("error",e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return flag;
	}

	public AccountVO getAccountDetails(int account_number) throws AccountDAOException
	{
		String userName = "root";
		String password = "";
		String url = "jdbc:mysql://localhost:3306/sample";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccountVO vo = null;
		try {
			Driver driver = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(driver);
			connection = DriverManager.getConnection(url, userName, password);
			String query = "select * from account where account_number=?";
			ps = connection.prepareStatement(query);
			ps.setInt(1, account_number);
			rs = ps.executeQuery();
			while(rs.next()) {
				vo = new AccountVO();
				vo.setAccount_number(rs.getInt("account_number"));
				vo.setCustomer_name(rs.getString("customer_name"));
				vo.setContact_no(rs.getInt("contact_no"));
				vo.setAadhar_no(rs.getInt("aadhar_no"));
				vo.setCustomer_dob(rs.getDate("customer_dob"));
				vo.setAccount_balance(rs.getDouble("account_balance"));
			}
			if(vo==null)
				throw new AccountDAOException("Given account number is not there");
			
		} catch (SQLException e) {
			throw new AccountDAOException("Erro in sql ",e);
		}
			finally
			{
				if(connection!=null)
				{
					try {
						connection.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return vo;
		}
	

}
