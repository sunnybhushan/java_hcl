package accountMain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import com.exception.AccountBOException;

import accountService.AccountService;
import accountVO.AccountVO;

public class AccountMain {

	public static void main(String[] args) throws AccountBOException, ParseException {
		System.out.println("Please select the one of the below option");
		System.out.println("1. Add account");
		System.out.println("2. Fetch account details by account number");
		System.out.println("3. Exit");
		Scanner sc = new Scanner(System.in);
		int menu = sc.nextInt();
		switch (menu) {
		case 1:
			storeAccountDetails();
		case 2:
			getAccountDetails();

		case 3:
			System.exit(0);

		}

	}

	private static void getAccountDetails() throws AccountBOException {
		AccountService service = new AccountService();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the account number for fetch");
		Integer account_number = Integer.parseInt(sc.nextLine());
		AccountVO vo;
		vo = service.getAccountDetails(account_number);
		if (vo != null) {
			System.out.println("==========================================================================");
			System.out.println("Account Number" + '\t' + "CustomerName" + '\t' + "Contact No" + '\t' + "Aadhar No"
					+ '\t' + "Customer dob" + '\t' + "Account Balance" + '\t');

			System.out.println(vo.getAccount_number() + "\t\t" + vo.getCustomer_name() + "\t\t" + vo.getContact_no()
					+ "\t\t" + vo.getAadhar_no() + "\t\t" + vo.getCustomer_dob() + "\t\t" + vo.getAccount_balance());
		}

	}

	private static void storeAccountDetails() throws ParseException {
		AccountService service = new AccountService();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Account number");
		int acc_no = sc.nextInt();
		System.out.println("Enter the Customer name");
		String cus_name = sc.nextLine();
		System.out.println("Enter the contact No");
		int contact = sc.nextInt();
		System.out.println("Enter the Aadhar No");
		int aadhar = sc.nextInt();
		System.out.println("Enter the Customer dob");
		String str = sc.nextLine();
		DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		java.util.Date date = formatter.parse(str);
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		System.out.println("Enter the Account Balance");
		double bal = sc.nextDouble();

		AccountVO vo = new AccountVO(acc_no, cus_name, contact, aadhar, sqlDate, bal);
		String obj = service.storeAccountDetails(vo);
		System.out.println(obj);

	}
}