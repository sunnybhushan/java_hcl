package accountService;

import com.exception.AccountBOException;

import accountBO.AccountBO;
import accountDAO.AccountDAO;
import accountVO.AccountVO;

public class AccountService {

	AccountBO bo = new AccountBO();
	
	public String storeAccountDetails(AccountVO vo) {
		
		String msg;
		boolean flag;
		try {
			flag = bo.storeAccountDetails(vo);
			if(flag)
				msg = "Account added successfully";
			else
				msg = "Error when adding to account";
			
		} catch (Exception e) {
			msg = e.getMessage();
		}
		return msg;
	}

	public AccountVO getAccountDetails(int account_number) throws AccountBOException {
		AccountVO vo = null;
		try {
			vo = bo.getAccountDetails(account_number);
		} catch (AccountBOException e) {
			e.printStackTrace();
			vo = null;
			
		}
		return vo;
	}
	
}
















