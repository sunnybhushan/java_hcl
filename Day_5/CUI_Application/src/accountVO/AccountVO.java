package accountVO;

import java.sql.Date;

public class AccountVO {

	private int account_number;
	private String customer_name;
	private int contact_no;
	private int aadhar_no;
	private Date customer_dob;
	private double account_balance;
	
	public AccountVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountVO(int account_number, String customer_name, int contact_no, int aadhar_no, Date customer_dob,
			double account_balance) {
		super();
		this.account_number = account_number;
		this.customer_name = customer_name;
		this.contact_no = contact_no;
		this.aadhar_no = aadhar_no;
		this.customer_dob = customer_dob;
		this.account_balance = account_balance;
	}

	public int getAccount_number() {
		return account_number;
	}

	public void setAccount_number(int account_number) {
		this.account_number = account_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public int getContact_no() {
		return contact_no;
	}

	public void setContact_no(int contact_no) {
		this.contact_no = contact_no;
	}

	public int getAadhar_no() {
		return aadhar_no;
	}

	public void setAadhar_no(int aadhar_no) {
		this.aadhar_no = aadhar_no;
	}

	public Date getCustomer_dob() {
		return customer_dob;
	}

	public void setCustomer_dob(Date customer_dob) {
		this.customer_dob = customer_dob;
	}

	public double getAccount_balance() {
		return account_balance;
	}

	public void setAccount_balance(double account_balance) {
		this.account_balance = account_balance;
	}

	
	@Override
	public String toString() {
		return "AccountBO [account_number=" + account_number + ", customer_name=" + customer_name + ", contact_no="
				+ contact_no + ", aadhar_no=" + aadhar_no + ", customer_dob=" + customer_dob + ", account_balance="
				+ account_balance + "]";
	}
}
