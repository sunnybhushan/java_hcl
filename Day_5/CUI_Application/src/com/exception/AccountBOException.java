package com.exception;

public class AccountBOException extends Exception{

	public AccountBOException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountBOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AccountBOException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AccountBOException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AccountBOException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
