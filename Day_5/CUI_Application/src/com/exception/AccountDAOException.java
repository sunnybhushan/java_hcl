package com.exception;

public class AccountDAOException extends Exception{

	public AccountDAOException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountDAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AccountDAOException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AccountDAOException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AccountDAOException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
