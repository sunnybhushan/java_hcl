/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.Map;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
	Map<String, String> m1 = new HashMap<String, String>();

	public void storeCountryCapital(String countryName, String capital) {
		m1.put(countryName, capital);
	}

	public void retrieveCapital(String countryName) {
		String capital = m1.get(countryName);
		System.out.println(capital);
	}

	public String retrieveCountry(String capitalName) {
		for (String key : m1.keySet()) {
			if (capitalName.equals(m1.get(key))) {
				return key;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		MapDemo obj = new MapDemo();
		obj.storeCountryCapital("US", "Washington");
		obj.storeCountryCapital("Russia", "Moscow");
		obj.storeCountryCapital("India", "New Delhi");

		obj.retrieveCapital("US");
		System.out.println(obj.retrieveCountry("New Delhi"));
	}

}
