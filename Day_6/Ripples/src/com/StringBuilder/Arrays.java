/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.StringBuilder;


public class Arrays {
public static void main(String[] args) {
	
	int []ar = new int[100];
	 ar[0] = 0;
	
	for (int i = 1; i <= 50; i++) {
		ar[i] = i*3;
	}
	
	for (int i = 0; i <= 50; i++) {
		System.out.print(ar[i]+" ");
	}
}
}
