/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.StringBuilder;

import java.util.Scanner;
import java.util.StringTokenizer;

public class StringFinder {

	public static void main(String[] args) {
		
		System.out.println("Enter the String for first method");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		StringBuilder st = new StringBuilder(str);
		// Method 1
		method1(st);
		System.out.println();
		// Method 2
		System.out.println("Enter the String for second method"); 
		String str2 = sc.nextLine();
		token(str2);
		System.out.println();
		// method 3
		System.out.println("Enter the String for third method");
		String str3 = sc.nextLine();
		strRevAndPrint(str3);
		
		
		
		
		
		
	}
	public static void method1(StringBuilder st) {
		for (int j = 0; j < st.length(); j++) {
			if (j % 2 == 1) {
				System.out.print(st.charAt(j));
			}
		}
		
	}
	public static void token(String str)
	{
		StringTokenizer token =  new StringTokenizer(str," ");
		while(token.hasMoreTokens()) {
			String s = token.nextToken();
		System.out.print(s.substring(0, 1).toUpperCase()+s.substring(1));
	    System.out.print(" ");
		}   
	}
	
	private static void strRevAndPrint(String str) {
		int index = str.length();
		char ch;
		String nstr="";
		if(index %2 ==0)
			System.out.println(str);
		else
		{
			for (int i=0; i<str.length(); i++)
		      {
		        ch= str.charAt(i);
		        nstr= ch+nstr; 
		      }
		    }
		
		System.out.println(nstr);
	}

}
