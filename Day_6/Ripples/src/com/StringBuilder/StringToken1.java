/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.StringBuilder;

import java.util.Scanner;
import java.util.StringTokenizer;

public class StringToken1 {

	public static void main(String[] args) {
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String a = sc.nextLine();
		// Problem 1:-
		StringTokenizer st = new StringTokenizer(a, " ");
		int count = 0;
		while (st.hasMoreTokens()) {
			String s = st.nextToken();
			count++;
			System.out.println(s.substring(0, 1).toUpperCase() + s.substring(1));
		}

		// Problem 2:-
		System.out.println("No of words: " + count);

		// Problem 3:-
		System.out.println("Enter the second string");
		String b = sc.nextLine();
		StringTokenizer st3 = new StringTokenizer(b);
		while (st3.hasMoreTokens()) {
			String str = st3.nextToken();
			String[] arrOfStr = str.split("-");

			for (int i = 0; i < arrOfStr.length; i++) {
				char ch = arrOfStr[i].charAt(0);

				if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I'
						|| ch == 'O' || ch == 'U') {
					System.out.print(arrOfStr[i]);
				}
			}
		}

	}
}
