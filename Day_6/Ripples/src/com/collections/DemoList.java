/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class DemoList {
	
	public static void main(String[] args) {
		
		List<Integer> al = new ArrayList<Integer>();
		al.add(1);
		al.add(2);
		al.add(3);
		al.add(4);
		System.out.println(al);
		al.remove(0);
		System.out.println(al);
		System.out.println("Size of arrayList: "+al.size());
		System.out.println("Third element: "+al.get(2));
		for (Integer integer : al) {
			System.out.println(integer);
		}
		System.out.println("Iterator method");
		Iterator<Integer> it = al.iterator();	
		 while (it.hasNext())
	            System.out.print(it.next() + " ");
		
		
		Students s1 = new Students(1, "Ram", 123);
		Students s2 = new Students(2, "Rama", 1235);
		Students s3 = new Students(3, "Ramesh", 1243);
		Students s4 = new Students(4, "Ramu", 12223);
		
		Set<Students> set1 = new HashSet<Students>();
		set1.add(s1);
		set1.add(s2);
		set1.add(s3);
		set1.add(s4);
		System.out.println("Get: ");
		//System.out.println(set1);
		
		HashMap<Integer, Students> hs = new HashMap<Integer, Students>();
		
		
		hs.put(1, s1);
		hs.put(2, s2);
		
		System.out.println("Map: "+hs.keySet()+"  "+ hs.values());
		
		//hs.putAll(Collections.addAll(1, set1));
		
		
		
	}

}
