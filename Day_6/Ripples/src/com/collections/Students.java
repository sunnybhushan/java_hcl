/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.collections;

public class Students {

	int id;
	String name;
	long mobileNo;
	
	public Students(int id, String name, long mobileNo) {
		super();
		this.id = id;
		this.name = name;
		this.mobileNo = mobileNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Override
	public String toString() {
		return "Students [id=" + id + ", name=" + name + ", mobileNo=" + mobileNo + "]";
	}
	
	
}
