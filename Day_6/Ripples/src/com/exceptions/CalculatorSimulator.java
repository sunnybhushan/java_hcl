/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.exceptions;

import java.util.Scanner;

public class CalculatorSimulator {

	
	public static void main(String[] args) throws Exception {
		TaxCalculator tc = new TaxCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Name");
		String name =sc.nextLine();
		System.out.println("Salary");
		double sal = sc.nextDouble();
		System.out.println("True or false");
		boolean isIndian = sc.nextBoolean();
		double tax = tc.calculateTax(name, isIndian, sal);
		System.out.println("Tax amount is: "+tax);
	}
}
