/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.exceptions;

public class CountryNotValidException extends Exception {

	
	public CountryNotValidException() {
		System.out.println("The employee should be an Indian citizen for calculating tax");
	}
	public CountryNotValidException(String message) {
		System.out.println("The employee should be an Indian citizen for calculating tax");
	}

}
