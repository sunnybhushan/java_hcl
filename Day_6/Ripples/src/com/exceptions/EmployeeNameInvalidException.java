/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.exceptions;

public class EmployeeNameInvalidException extends Exception {



	public EmployeeNameInvalidException(String message) {
		System.out.println("The employee name cannot be empty");
	}


}
