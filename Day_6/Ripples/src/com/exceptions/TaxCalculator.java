/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.exceptions;

public class TaxCalculator {

	public double calculateTax(String empName, boolean isIndian, double empSal) throws Exception{
		double taxAmount;
		if(isIndian == false) {
			throw new CountryNotValidException();
		}
	    if(empName == null)
	    {
	    	throw new EmployeeNameInvalidException("Invalid");
	    }
		
	    if(empSal> 100000 && isIndian==true)
	    {
	    	taxAmount = empSal * 8/100;
	    }
	    else if(empSal > 50000 && empSal < 100000 && isIndian==true) {
	    	taxAmount = empSal * 6/100;
	    }
	    
	    else if(empSal>10000 && empSal>30000 && isIndian == true) {
	    	taxAmount = empSal *4/100;
	    }
	    else
	    	throw new TaxNotEligibleException("Not eligible");
	    
		return taxAmount;
	}
}
