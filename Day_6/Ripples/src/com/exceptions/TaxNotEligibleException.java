/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.exceptions;

public class TaxNotEligibleException extends Exception {



	public TaxNotEligibleException(String message) {
		System.out.println("The employee does not need to pay tax");
	}

	

}
