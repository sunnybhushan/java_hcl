/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.interfaces;

public class AdultUsers implements LibraryUser{

	@Override
	public void registerAcoount(int age) {

		if(age>12)
			System.out.println("You have successfully registered under Adult Account");
		else
			System.out.println("Sorry, Age must be greater than 12 as a adult");
	}

	@Override
	public void requestBook(int bookType) {
		if(bookType==2)
			System.out.println("Book issused successfully, please return the book within 7 days");
		else
			System.out.println("Oops, you are allowed to take only adult Fiction books");
		
	}

}
