/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.interfaces;

public class KidUser implements LibraryUser {

	@Override
	public void registerAcoount(int age) {

		if(age<12)
			System.out.println("You have successfully registered under kids Account");
		else
			System.out.println("Sorry, Age must be less than 12 to register as a kid");
	}

	@Override
	public void requestBook(int bookType) {
		
		if(bookType==1)
			System.out.println("Book issused successfully, please return the book within 10 days");
		else
			System.out.println("Oops, you are allowed to take only kids books");		
	}

	
}
