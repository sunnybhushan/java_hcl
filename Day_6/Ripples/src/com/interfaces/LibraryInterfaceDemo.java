/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.interfaces;

public class LibraryInterfaceDemo {

	public static void main(String[] args) {
		KidUser kid = new KidUser();
		kid.registerAcoount(10);
		kid.registerAcoount(15);
		kid.requestBook(1);
		kid.requestBook(2);
		System.out.println("---------------");
		AdultUsers adult = new AdultUsers();
		adult.registerAcoount(10);
		adult.registerAcoount(15);
		adult.requestBook(2);
		adult.requestBook(1);
	}
}
