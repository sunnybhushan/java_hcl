/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.interfaces;

public interface LibraryUser {

	void registerAcoount(int age);
	
	void requestBook(int bookType);
}
