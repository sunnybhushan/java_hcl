/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EvenNumberGenerator {

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int no = sc.nextInt();
		NumberGeneratorImpl ng = (n) ->{
			for (int i = 1; i <= n; i++) {
				System.out.print(i+ ", ");
			}
			System.out.println();
		};
		ng.generate(no);
		
		System.out.println("-------------Next Question-------------");
		List<String> ls = new ArrayList<String>();
		ls.add("India");
		ls.add("China");
		ls.add("Pakistan");
		ls.add("Sri Lanka");
		ls.add("Indonesia");
		ls.add("Brazil");
		ls.add("Nepal");
		ls.add("Bhutan");
		ls.add("Myanmar");
		ls.add("Russia");
		
		ls.forEach((n)->{
			  CharSequence seq = "s";
			    boolean bool = n.contains(seq);
			   if(bool)
				   System.out.println(n);
			
		});
		
	}
}
