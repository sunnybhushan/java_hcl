/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaFunction;

public interface NumberGeneratorImpl {

	public void generate(int n);
	
}
