/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaStrig;

import java.util.Scanner;

public class String1 {

	public static void main(String[] args) {
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
// Question 1
		for (int i = 0; i < str.length(); i++) {
			System.out.println(str.charAt(i));
		}

// Question 2
		StringBuffer sb = new StringBuffer(str);
		sb.reverse();
		System.out.println("String Reverse: " + sb);

// Question 3
		char[] ch = str.toCharArray();
		System.out.println("String reverse using for loop");
		for (int i = ch.length - 1; i >= 0; i--) {
			System.out.print(ch[i] + " ");
		}
// Question 4
		System.out.println("Enter the sentence");
		String sentence = sc.nextLine();

		StringBuffer newStr = new StringBuffer(sentence);

		for (int i = 0; i < sentence.length(); i++) {

			if (Character.isLowerCase(sentence.charAt(i))) {
				newStr.setCharAt(i, Character.toUpperCase(sentence.charAt(i)));
			} else if (Character.isUpperCase(sentence.charAt(i))) {
				newStr.setCharAt(i, Character.toLowerCase(sentence.charAt(i)));
			}
		}
		System.out.println("String after case conversion : " + newStr);
	}
}
