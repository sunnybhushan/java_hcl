/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaStrig;

import java.util.Scanner;

public class String2 {

	public static void main(String[] args) {

		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		// Question 1
		System.out.println("forth character is :" + str.charAt(3));
		// Question 2
		System.out.println("first occurrence: " + str.indexOf("l"));
		// Question 3
		System.out.println("Is string empty: " + str.isEmpty());
		// Question 4
		System.out.println("last occurrence: " + str.lastIndexOf("l"));
		// Question 5

		System.out.println("Enter the String using |");
		String str1 = sc.next();

		System.out.println("Replace: " + str1.replace('|', ' '));
		// Question 6
		System.out.println("SubStr: " + str.substring(str.length() - 3));
		// Question 7
		System.out.println("Upper Case: " + str1.toUpperCase());
		// Question 8
		String str2 = "";
		if (str2 == null || str2.isEmpty() || str2.trim().isEmpty())
			System.out.println("String is null.");
		else
			System.out.println("String is not null");
	}
}
