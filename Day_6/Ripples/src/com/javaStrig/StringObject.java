/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaStrig;

import java.util.Scanner;

public class StringObject {

	public static void main(String[] args) {
		
		System.out.println("Enter the string 1");
		Scanner sc = new Scanner(System.in);
		String str1 = sc.next();
		System.out.println("Enter the string 2");
		String str2 = sc.next();
		// Question 1:-
		System.out.println("Concate two String: "+str1+str2);
		// Question 2:-
		boolean bol;
		if(str1.equals(str2)) {
			bol = true;
			System.out.println("String is equal: "+bol);
		}
		else {
			bol = false;
			System.out.println("String is not equal: "+bol);
		}
		
		// Question 3:-
		System.out.println("Concate first three charactere: "+str1.substring(0, 3)+(str2.substring(0, 3)));
	
		// Question 4:-
		System.out.println("Length of first string: "+ str1.length());
		
	}
}
