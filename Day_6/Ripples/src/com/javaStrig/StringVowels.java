/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.javaStrig;

import java.util.Scanner;

public class StringVowels {

	public static void main(String[] args) {
		// Question 1:-
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine().toLowerCase();
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == ' ') {
				count++;
			}
		}
		System.out.println("Number of vowels: " + count);
		// Question 2:-
		int count1 = 0;
		for (int i=0 ; i<str.length(); i++){
	         char ch1 = str.charAt(i);
	         if(ch1 == 'a' || ch1 == 'e' || ch1 == 'i' || ch1 == 'o' || ch1 == 'u' ){
	            System.out.print("");
	         }else if(ch1 != ' '){
	            count1++;
	         }
	      }
		System.out.println("Number of consonent: " + count1);
		
	}

}
