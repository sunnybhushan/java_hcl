/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.java_Streams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Program {

	public static void main(String[] args) {
		// Problem statement 1:
		List<String> ls1 = new ArrayList<String>();
		ls1.add("USA");
		ls1.add("China");
		ls1.add("India");
		ls1.add("UK");

		List<Character> l1 = ls1.stream().map(x -> x.charAt(x.length() - 1)).collect(Collectors.toList());
		// l1.forEach(System.out::println);
		System.out.println("First character: " + l1);

		// Problem statement 2:

		List<String> ls2 = new ArrayList<String>();
		ls2.add("USA");
		ls2.add("China");
		ls2.add("India");
		ls2.add("UK");

		String l2 = String.join(",", ls2);
		System.out.println("Concate String: " + l2);

		// Problem statement 3:

		List<String> ls3 = new ArrayList<String>();
		ls3.add("USA");
		ls3.add("China");
		ls3.add("India");
		ls3.add("UK");
		ls3.add("Uganda");

		List<String> l3 = ls3.stream().map(str -> str.substring(0, 2)).collect(Collectors.toList());
		System.out.println("First two character: " + l3);

		List<Boolean> match = l3.stream().map(s -> s.contains("U")).collect(Collectors.toList());

		System.out.println("result: " + match);

		// Problem Statement 4

		List<String> ls4 = new ArrayList<String>();
		ls4.add("USA");
		ls4.add("China");
		ls4.add("India");
		ls4.add("UK");
		ls4.add("USA");

		List<String> l4 = ls4.stream().distinct().collect(Collectors.toList());
		System.out.println("Remove dublicate: " + l4);

		// Problem statement 5

		Map<String, Integer> mp5 = new HashMap<String, Integer>();
		mp5.put("Tom", 89);
		mp5.put("Raj", 45);
		mp5.put("Jack", 39);
		mp5.put("Rama", 93);

		IntSummaryStatistics d = mp5.entrySet().stream().mapToInt(x -> x.getValue()).summaryStatistics();

		System.out.println("Average math marks: " + d.getAverage());
		System.out.println("Maximum math marks: " + d.getMax());
		System.out.println("Lowest math marks: " + d.getMin());
		System.out.println("Total math marks: " + d.getSum());

	}
}
