/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class EvenNumberGenerator {

	static List<Integer> evenNumers;
	public static void main(String[] args) {
		System.out.println("Enter the numbers");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		evenNumers = new ArrayList<Integer>();
		
		EvenNumberGenerator m = new EvenNumberGenerator();
		m.storeEvenNumber(n);
		m.printEvenNumbers(evenNumers);
		System.out.println("Retrieve: "+m.retrieveEvenNumber(4));
		System.out.println("Retrieve: "+m.retrieveEvenNumber(9));
		
	}
	public void storeEvenNumber(int n)
	{
		for(int i=1;i<n; i++)
		{
			if(i%2==0)
				evenNumers.add(i);
		}
	}
	public void printEvenNumbers(List<Integer> evenNo)
	{
		for (Integer object : evenNo) {
			System.out.print(object*2+" ");
			
		}
		System.out.println("    ");
	 }
	          
	public int retrieveEvenNumber(int n) {
	
		 for (int i = 0; i < evenNumers.size(); i++)
		 {  
			int a = evenNumers.get(i);
			if(a==n)
				return a;
	      }
		 
		return 0;
		
	}
    
}













