/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.methodReference;

import java.util.StringJoiner;

public class Colors {

	public static void main(String[] args) {
		
	
	StringJoiner sj = new StringJoiner(",", "[Colors:","]");
	sj.add("Red");
	sj.add("Green");
	sj.add("Yellow");
	sj.add("Pink");
	System.out.println(sj);
	
	joinStudentNames("Jack", "John", "Tim", "Tom");
	
}
	public static void joinStudentNames(String...values)
	{
		StringJoiner sj2 = new StringJoiner("-");
		for (String i: values)
			sj2.add(i);
		System.out.println(sj2);
	}
	
}