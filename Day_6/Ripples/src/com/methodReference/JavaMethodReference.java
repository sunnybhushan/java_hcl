/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.methodReference;

public class JavaMethodReference {

	public void sayHello() {
		System.out.println("Say hello");
	}
	
	public static void main(String[] args) {
		JavaMethodReference ref = new JavaMethodReference();
		Say say = ref:: sayHello;
		say.wish();
	}
}
