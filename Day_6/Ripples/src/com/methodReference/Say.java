/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.methodReference;

@FunctionalInterface
public interface Say {
	
	
	public void wish();

}
