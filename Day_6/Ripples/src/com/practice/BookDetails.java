/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.practice;

import java.util.*;

public class BookDetails {

	public static void main(String[] args) {
		
		Map<String,BookVO> m1 = new HashMap<String, BookVO>();
		
		m1.put("Tamil", new BookVO("Java", "Abc", 1250));
		m1.put("English", new BookVO("Angular", "Abc", 63000));
		m1.put("Hindi", new BookVO("React", "John", 10));
		
		System.out.println(m1);
		
		
		 
	}
}
