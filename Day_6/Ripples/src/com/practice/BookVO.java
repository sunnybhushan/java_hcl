/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.practice;

public class BookVO {

	private String bookName;
	private String authorName;
	private int value;
	
	public BookVO(String bookName, String authorName, int value) {
		super();
		this.bookName = bookName;
		this.authorName = authorName;
		this.value = value;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BookVO [bookName=" + bookName + ", authorName=" + authorName + ", value=" + value + "]";
	}
	
}
