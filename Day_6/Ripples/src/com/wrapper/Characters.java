/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.wrapper;

public class Characters {

	public static void main(String[] args) {
		char ch = 'b';
		char ch3 = Character.toUpperCase(ch); 
		System.out.println("Upper case: "+ch3);
		
		char ch2 = new Character('c');
		System.out.println("Lower: "+Character.isLowerCase(ch2));
		
		System.out.println("Number: "+Character.isDigit(ch2));
		
		System.out.println("Space: "+Character.isWhitespace(ch2));
	}
}
