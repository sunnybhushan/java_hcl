/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.wrapper;

public class Doubles {
	public static void main(String[] args) {
	
		double d = 20004.345;
		int i = (int) d;
		System.out.println("Integer: "+i);
		
		double d1 = 1457.89;
		double d2 = new Double(d1);
		System.out.println("Double: "+d2);
		
		 Double d5 =Double.max(d1,d2); 
		 System.out.println("Max value: "+d5);
		
		String s=Double.toString(d1); 
		System.out.println("String: "+s);
	}
}
