/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.wrapper;

public class ExceptionDemo {

	int a1[] = new int[10];
	
	public void loadArray()
	{
		for (int i = 0; i < 10; i++) {
			a1[i]=i;
		}
	}
	
	public void printArrayElement(int index)
	{
		try {
			System.out.println(a1[index]);
		} catch (ArrayIndexOutOfBoundsException e) {
			
			System.out.println("Array index is wrong");
		}
	}
	
	public static void main(String[] args) {
		ExceptionDemo ed = new ExceptionDemo();
		ed.loadArray();
		ed.printArrayElement( 11);
	}
	
	
}
