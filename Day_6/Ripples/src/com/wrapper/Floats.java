/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.wrapper;

public class Floats {
	public static void main(String[] args) {
		float f1 = 123.45f;
		float f2 = 148.65f;
		
		Float f = new Float(f1);
		System.out.println("float object: "+f);
		Float ff = new Float(f2);
		if(f.equals(ff))
			System.out.println("equal");
		else
			System.out.println("Not equal");
		
		String str = Float.toHexString(f);
	      System.out.println("Hex String = " + str);
	}

}
