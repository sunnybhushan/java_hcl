/**
* @author  Sunny Bhushan
* @since   2022-01-12 
*/
package com.wrapper;

public class WrapperAPI {
	
public static void main(String[] args) {
	String s = "100";
	int a = new Integer(s);
	System.out.println("Integer: "+a);
	
	String st = "1234443";
	long l = new Long(st);
	System.out.println("Long: "+l);
	
	  Integer obj = new Integer("2450");
	  String ss=Integer.toString(obj);
	  System.out.println("String: "+ss);
	  System.out.println("octual: "+Integer.toOctalString(obj));
	  
	  double d = new Double(st);
	  System.out.println("Double: "+d);
}
}
